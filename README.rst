MPIFX - Modern Fortran Interface for MPI
========================================

The MPIFX project is devoted to create **modern Fortran interfaces** for
the MPI library.

It contains only a few routines for so far, but if those happen the ones
you need, feel free to use them (MPIFX is licensed under the **simplified BSD
license**).

If your routine is not wrapped yet, you could wrap it yourself and contribute it
to the project to enable to cover the target library sooner.
